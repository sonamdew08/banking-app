import React from "react";
// import Pagination from "react-js-pagination";
import '../../App.css';
import Paginations from "../Pagination/index"

class BankDetails extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            bankDetail: [],
            fetch: false,
            activePage: 1,
            startIndex: 0,
            endIndex: 50,
            pageSize: 50
        }
        this.getBankDetail = this.getBankDetail.bind(this);
    }

    componentDidMount(){
        fetch('https://vast-shore-74260.herokuapp.com/banks?city=MUMBAI')
        .then(response => response.json())
        .then(response => {
            this.setState({bankDetail: response, fetch: true}, () => {
                console.log("data", this.state.bankDetail)
            })
        })
    }

    getBankDetail(activePage){
        console.log("inside bankdetail", activePage)
        this.setState((prevState) => ({
            activePage: activePage,
            startIndex: (activePage - 1) * prevState.pageSize,
            endIndex: ((activePage - 1) * prevState.pageSize) + prevState.pageSize
        }), () => console.log("start " + this.state.startIndex + "end " + this.state.endIndex))

    }
   
    render(){
        if(this.state.fetch){
            var bankList = this.state.bankDetail.slice(this.state.startIndex, this.state.endIndex)
        }
        return (
            <div>
                {this.state.fetch ?
                <div>
                <table className = 'bank-info'>
                    <thead>
                        <tr>
                            <th>Bank Name</th>
                            <th>IFSC Code</th>
                            <th>branch</th>
                            <th>City</th>
                        </tr>                        
                    </thead>
                    <tbody>
                    {bankList.map((item, index) => (
                    <tr key={index}>
                        <td>{item.bank_name}</td>
                        <td>{item.ifsc}</td>
                        <td>{item.branch}</td>
                        <td>{item.city}</td>
                    </tr>
                ))}
                    </tbody>
                
                </table>
                
                <Paginations 
                    onHandleChange={this.getBankDetail}
                    perPage={this.state.pageSize}
                    totalCount={this.state.bankDetail.length} 
                    pageRange = {15}                   
                />
                </div>
                :
                <p>Loading...</p>
                }
            </div>
        );
    }
}

export default BankDetails;