import React from "react";

export default class Paginations extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            start: 1,
            pagerange: this.props.pageRange,
            perPage: this.props.perPage,
            totalCount: this.props.totalCount,
            active: ''
        }
    }
    
    handleClick = (event) => {
        const pagination = Math.floor(this.state.totalCount / this.state.perPage);
        let currentActivePage = event.currentTarget.dataset.id
        
        if (currentActivePage < (this.state.pagerange / 2) + 1 ) {
            this.setState({start: 1}, () => {
                this.props.onHandleChange(currentActivePage)})
        } 
        else if (currentActivePage >= (pagination - (this.state.pagerange / 2) )) {
            this.setState({start:  Math.floor(pagination - this.state.pagerange + 1)}, () => {
                this.props.onHandleChange(currentActivePage)})    
        } 
        else {
            this.setState({start: (currentActivePage - Math.floor(this.state.pagerange / 2))}, () => {
                this.props.onHandleChange(currentActivePage)})
        }
        
    }

    render(){       
        
        const NumToDisplay = []
        for(let i = this.state.start; i <= ((this.state.start + this.state.pagerange) - 1); i++){
            NumToDisplay.push(i)
        }
        console.log(NumToDisplay)
        return (
            
            <div className="pagination-div">
                <table className="pagination">
                    <tr>
                    { NumToDisplay.map((num, index) => (
                        <td className='pagination-li' id={index} key={index} data-id={num}  onClick={this.handleClick}>{num}</td>
                    ))}
                    </tr>                    
                </table>
            </div>
        );

    }
}