import React from 'react';
import logo from './logo.svg';
import './App.css';
import BankDetails from "./Component/BankDetails/index"

class App extends React.Component{
    render(){
        return (
            <BankDetails />
        );
    }
}

export default App;
